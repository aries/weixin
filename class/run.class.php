<?php
//微信公众平台控制器
class Wx_Run{
    /**
     * 数据
     *
     * @access private
     * @var mixed
     */
    private $_posts;
    /**
     * 析构函数
     *
     * @access public
     */
    public function __construct() {}
    /**
     * 获取POST的数据
     *
     * @access private
     * @return array
     */
    private function _input(){
        $datas = file_get_contents('php://input');
        if(!empty($datas)){
            $this->_posts = simplexml_load_string($datas, 'SimpleXMLElement', LIBXML_NOCDATA);
            return $this->_posts;
        }
        return false;
    }
    /**
     * 系统提示
     *
     * @access public
     * @return void
     */
    private function _tip(){
        $tpl = data::get('config.tpl.text');
        $result = sprintf($tpl, $this->_posts->FromUserName, $this->_posts->ToUserName, time(),'啥也没找到，要不您换个词儿？');
        echo $result;
        unset($tpl ,$result);
        exit;
    }
    /**
     * 输出信息
     *
     * @access public
     * @return void
     */
    public function response(){
        $posts = $this->_input();
        if($posts){
            $msgType = $posts->MsgType;
            require __CLASS_ROOT__.'/text.class.php';
            switch($msgType){
                case 'event':
                    if($posts->Event == 'subscribe'){
                        //发送帮助
                        $params = array('keyword'=>'help');
                        $text = new Wx_Text($params);
                        $data = $text->execute();
                        $tpl = data::get('config.tpl.text');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['content']);
                        unset($params ,$text,$data,$text);
                    }
                    break;
                case 'text':
                    $keyword = trim($posts->Content);
                    $params = array('keyword'=>$keyword);
                    $text = new Wx_Text($params);
                    $data = $text->execute();
                    if('text'==$data['type']){
                        if(empty($data['content'])) $this->_tip();
                        $tpl = data::get('config.tpl.text');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['content']);
                    }elseif('music'==$data['type']){
                        if(empty($data['content'])) $this->_tip();
                        $music = $data['content'];
                        if(is_array($music) && !empty($music)){
                            $tpl = data::get('config.tpl.music');
                            $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$music['title'],'按住可转发给好友!',$music['musicurl'],$music['hqmusicurl']);
                        }else{
                            $this->_tip();
                        }
                        unset($music);
                    }elseif('book'==$data['type']){
                        if(empty($data['xml'])) $this->_tip();
                        $tpl = data::get('config.tpl.news');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['counts'],$data['xml']);
                    }elseif('english'==$data['type']){
                        if(empty($data['content'])) $this->_tip();
                        $tpl = data::get('config.tpl.text');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['content']);
                    }elseif('astro'==$data['type']){
                        if(empty($data['content'])) $this->_tip();
                        $tpl = data::get('config.tpl.text');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['content']);
                    }elseif('joke'==$data['type']){
                        if(empty($data['content'])) $this->_tip();
                        $tpl = data::get('config.tpl.text');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['content']);
                    }elseif('express'==$data['type']){
                        if(empty($data['content'])) $this->_tip();
                        $tpl = data::get('config.tpl.text');
                        $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['content']);
                    }

                    unset($params,$keyword,$data,$text,$tpl);
                    break;
                case 'location':
                    require __CLASS_ROOT__.'/location.class.php';
                    $params = array('func'=>'businesses','x'=>$posts->Location_X,'y'=>$posts->Location_Y,'scale'=>$posts->Scale,'label'=>$posts->Label);
                    $location = new Wx_Location($params);
                    $data = $location->execute();
                    $tpl = data::get('config.tpl.location');
                    $result = sprintf($tpl, $posts->FromUserName, $posts->ToUserName, time(),$data['counts'],$data['xml']);
                    unset($params,$content,$text,$tpl,$data);
                    break;
            }
            echo $result;
        }
    }
}
<?php
//微信公众平台
//define your token
define('DEBUG',1);
if(DEBUG==0){
    error_reporting(0);
}else{
    error_reporting( E_ALL ^ E_NOTICE );//除了notice提示，其他类型的错误都报告
}
session_start();
define('__ROOT__' ,dirname(__FILE__));
define('__LIB_ROOT__' ,__ROOT__.'/lib');
define('__CLASS_ROOT__' ,__ROOT__.'/class');
define('__DATA_ROOT__' ,__ROOT__.'/data');
header ("Content-type: text/html;charset=utf-8");
//载入支持库
require __LIB_ROOT__ . '/functions.php';
if (function_exists('get_magic_quotes_gpc') && !get_magic_quotes_gpc()) {
    $_GET = addslashesDeep($_GET);
    $_POST = addslashesDeep($_POST);
    $_REQUEST = addslashesDeep($_REQUEST);
    $_COOKIE = addslashesDeep($_COOKIE);
    reset($_GET);
    reset($_POST);
    reset($_COOKIE);
}
//载入缓存库
require __LIB_ROOT__ . '/data.php';
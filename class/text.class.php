<?php
//文字
if(!defined('__ROOT__')) die ("Access Denied.");
class Wx_Text{
    /**
     * 相关参数
     *
     * @access private
     */
    private $_params = array();
    /**
     * 析构函数
     *
     * @access public
     */
    public function __construct($params) {
        $this->_params = $params;
    }
    /**
     * 执行操作
     *
     * @access public
     */
    public function execute(){
        $keyword = $this->_params['keyword'];
        $result = array('type'=>'text','content'=>'请输入点什么吧，比如：帮助、天气@城市名...');
        if(empty($keyword)){
            return $result;
        }
        $split = data::get('config.split');
        if(strpos($keyword,$split)){
            list($key,$data) = explode($split ,$keyword);
            switch(strtolower($key)){
                case '天气':
                case 'tq':
                    $result['content'] = $this->_weather($data);
                    $result['type'] = 'text';
                    break;
                case '音乐':
                case 'yy':
                    $result['content'] = $this->_music($data);
                    $result['type'] = 'music';
                    break;
                case '小说':
                case 'xs':
                    $result['xml'] = $this->_book($data);
                    $result['counts'] = 1;
                    $result['type'] = 'book';
                    break;
                case '星座':
                case 'xz':
                    $result['content'] = $this->_astro($data);
                    $result['type'] = 'astro';
                    break;
                case '快递':
                    $result['content'] = $this->_express($data);
                    $result['type'] = 'express';
                    break;
            }
        }else{
            switch(strtolower($keyword)){
                case '帮助':
                case 'help':
                    $result['content'] = "使用帮助说明：\r\n-----------------\r\n"
                        . "常用命令：\r\n"
                        . "1、帮助或help;\r\n"
                        . "2、天气@城市名或者tq@城市名;\r\n"
                        . "3、小说@书名或者xs@书名;\r\n"
                        . "4、音乐@歌名或者yy@歌名;\r\n"
                        . "5、发送地理位置，可查询附近的商家;\r\n"
                        . "6、英语或者en;\r\n"
                        . "7、星座@星座名或者xz@星座名;\r\n"
                        . "8、笑话或者讲个笑话或者再讲一个;\r\n"
                        . "9、快递@单号;\r\n"
                        . "-----------------\r\n如果喜欢就推荐给您的朋友，\r\n我们的微信号：".data::get('config.account');
                    break;
                case '英语':
                case 'en':
                    $result['content'] = $this->_english();
                    $result['type'] = 'english';
                    break;
                case '笑话':
                case '讲个笑话':
                case '再讲一个':
                    $result['content'] = $this->_joke();
                    $result['type'] = 'joke';
                    break;

            }
        }
        return $result;
    }
    /**
     * 天气
     *
     * @access public
     */
    private function _weather($keyword){
        $weathers = data::get('weather');
        if(isset($weathers[$keyword])){
            $code = $weathers[$keyword];
            $url = "http://m.weather.com.cn/data/".$code.".html";
            $file = file_get_contents($url);
            $obj = json_decode($file);
            $weatherinfo = $obj->weatherinfo;
            $city = $weatherinfo->city;
            $temp1 = $weatherinfo->temp1;
            $img1 = $weatherinfo->img1;
            $weather1 = $weatherinfo->weather1;
            $wind1 = $weatherinfo->wind1;
            $index = $weatherinfo->index;
            $index_d = $weatherinfo->index_d;
            $date_y = $weatherinfo->date_y;
            $week = $weatherinfo->week;
            $index_cl = $weatherinfo->index_cl;
            $index_ag = $weatherinfo->index_ag;
            $index_ls = $weatherinfo->index_ls;
            $content = "天气预报：\r\n-----------------\r\n"
                . "城市：{$keyword}\r\n"
                . "日期：{$date_y}，{$week}\r\n"
                . "天气：{$weather1}，{$temp1}，{$wind1}\r\n"
                . "穿衣指数：{$index}，{$index_d}\r\n"
                . "{$index_cl}晨练，{$index_ls}晾晒，{$index_ag}过敏";
            return $content;
        }
    }
    /**
     * 音乐
     *
     * @access public
     */
    private function _music($song){
        if(!empty($song)){
            $api = 'http://api2.sinaapp.com/search/music/?appkey=0020130430&appsecert=fa6095e1133d28ad&reqtype=music&keyword='.urlencode($song);
            $http = file_get_contents($api);
            if(!empty($http)){
                $json = json_decode($http,true);
                if($json['errcode']=='0'){
                    return $json['music'];
                }
            }
        }
        return ;
    }
    /**
     * 小说
     *
     * @access public
     */
    private function _book($name){
        if(!empty($name)){
            $api = 'http://k.soso.com/adr/search.jsp?icfa=1304450&sid=Ac_aOJYValkFy__d-xORztdy&g_ut=3&key='.urlencode($name).'&st=input&icfa=1334008';
            $data = curl_get($api);
            if(!empty($data)){
                list($head,$body) = explode('<ol id="result">',$data);
                list($body) = explode('<div id="page">',$body);
                preg_match("/<li>(.+?)<\/li>/is" ,$body,$mat);
                if(!empty($mat[1])){
                    preg_match("/href=\"(.+?)\"/is",$mat[1],$url);
                    $url = 'http://k.soso.com'.$url[1];
                    $url = str_replace('&amp;','&',$url);
                    $data = curl_get($url);
                    if(!empty($data)){
                        preg_match("/<div class=\"img\"><img src=\"(.+?)\" alt=\"SOSO小说\" \/>/is",$data ,$img);
                        preg_match("/<div class=\"sumCont\">(.+?)<\/div>/is",$data,$sum);
                        preg_match("/<h3 class=\"secTit\">(.+?)<\/h3>/is",$data,$title);
                        $tpl = data::get('config.tpl.item');
                        $intro = strip_tags($sum[1]);
                        return sprintf($tpl ,$title[1],$intro,$img[1],$url);
                    }
                }
            }
        }
        return ;
    }
    /**
     * 每日英语
     *
     * @access public
     */
    private function _english(){
        $api = 'http://dict.hjenglish.com/rss/daily/en';
        $http = file_get_contents($api);
        if($http){
            $xml = simplexml_load_string($http, 'SimpleXMLElement', LIBXML_NOCDATA);
            if($xml){
                $content = "句子：".$xml->channel->item->title."-----------------"
                . "翻译：".$xml->channel->item->cn_sentence."\r\n-----------------\r\n"
                . "日期：".$xml->channel->item->pubDate;
                return $content;
            }
        }
        return ;
    }
    /**
     * 星座
     *
     * @access public
     */
    private function _astro($data){
        $astro = data::get('astro');
        $data = str_replace('座','',$data);
        if(isset($astro[$data])){
            $api = 'http://vip.astro.sina.com.cn/astro/view/'.$astro[$data].'/day/';
            $http = file_get_contents($api);
            if($http){
                preg_match("/<div class=\"lotstars\" style=\"width: 580px;\">(.+?)<!-- 星座运势内容end -->/is" ,$http,$mat);
                $astar = $this->_star('综合运势',$mat[1]);
                $bstar = $this->_star('爱情运势',$mat[1]);
                $cstar = $this->_star('工作状况',$mat[1]);
                $dstar = $this->_star('理财投资',$mat[1]);
                preg_match("/div class=\"lotconts\">(.+?)<\/div>/",$mat[1],$intro);
                $content = $data."座今日运势-----------------\r\n";
                $content .= "综合运势：".$astar."\r\n"
                    . "爱情运势：".$bstar."\r\n"
                    . "工作状况：".$cstar."\r\n"
                    . "理财投资：".$dstar."\r\n"
                    . "健康指数：".$this->_theindex('健康指数',$mat[1])."\r\n"
                    . "商谈指数：".$this->_theindex('商谈指数',$mat[1])."\r\n"
                    . "幸运颜色：".$this->_theindex('幸运颜色',$mat[1])."\r\n"
                    . "幸运数字：".$this->_theindex('幸运数字',$mat[1])."\r\n"
                    . "速配星座：".$this->_theindex('速配星座',$mat[1])."\r\n"
                    . "今日概述：".$intro[1]."\r\n";
                unset($astro,$bstar,$cstar,$dstar,$mat,$intro);
                return $content;
            }

        }
        return ;
    }
    /**
     * 星级
     *
     * @access public
     */
    private function _star($name ,$mat){
        $star = '★';
        preg_match_all("/<h4>{$name}<\/h4><p><img src=\"(.+?)\" width=\"18\" height=\"18\" \/><\/p>/is" ,$mat,$stars);
        if($stars[0][0]){
            $args = explode('<img' ,$stars[0][0]);
            $counts = count($args) - 1;
            if($counts>0){
                $output = '';
                for($i=1;$i<=$counts;$i++){
                    $output.=$star;
                }
                return $output;
            }
        }
        return ;
    }
    /**
     * 星座指数
     *
     * @access public
     */
    private function _theindex($name ,$mat){
        preg_match("/<h4>{$name}<\/h4><p>(.+?)<\/p>/is" ,$mat ,$index);
        return $index[1];
    }
    /**
     * 笑话
     *
     * @access public
     */
    private function _joke(){
        $api = 'http://api100.duapp.com/joke/?appkey=0020130430&appsecert=fa6095e113cd28fd';
        $http = file_get_contents($api);
        if($http){
            return $http;
        }
        return ;
    }
    /**
     * 快递
     *
     * @access public
     */
    private function _express($data){
        $api = 'http://api100.duapp.com/express/?appkey=0020130430&appsecert=fa6095e113cd28fd&number='.$data;
        $http = file_get_contents($api);
        if($http){
            $args = explode('\n',$http);
            if(is_array($args) && !empty($args)){
                $output = '';
                foreach($args as $v){
                    $output .= $v."\r\n-----------------\r\n";
                }
                return $output;
            }
        }
        return ;
    }
}
<?php
if(!defined('__ROOT__')) die ("Access Denied.");
return array(
    '白羊'=>'aries',
    '金牛'=>'taurus',
    '双子'=>'gemini',
    '巨蟹'=>'cancer',
    '狮子'=>'leo',
    '处女'=>'virgo',
    '天秤'=>'libra',
    '天蝎'=>'scorpio',
    '射手'=>'sagittarius',
    '魔羯'=>'capricorn',
    '水瓶'=>'aquarius',
    '双鱼'=>'pisces'
);
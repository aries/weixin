<?php
if(!defined('__ROOT__')) die ("Access Denied.");
function check_signature(){
    $signature = $_GET["signature"];
    $timestamp = $_GET["timestamp"];
    $nonce = $_GET["nonce"];
    $token = data::get('config.token');
    $tmpArr = array($token, $timestamp, $nonce);
    sort($tmpArr);
    $tmpStr = implode( $tmpArr );
    $tmpStr = sha1( $tmpStr );
    if( $tmpStr == $signature ){
        return true;
    }else{
        return false;
    }
}
function valid() {
    if(check_signature()){
        echo $_GET["echostr"];
        exit;
    }
}
function addslashesDeep($value){
    return is_array($value) ? array_map('addslashesDeep', $value) : addslashes($value);
}
function table($table ,$as = ''){
    $table = '`'.data::get('config.db.prefix').$table.'`';
    if($as){
        $table .= ' AS '.$as;
    }
    return $table;
}
function set_log($name ,$content ,$append = 1){
    $name = __ROOT__.'/logs/'.$name;
    if($append){
        file_put_contents($name ,$content , FILE_APPEND);
    }else{
        file_put_contents($name ,$content);
    }
}
/**
 * POST data
 */
function curl_post($url,$data){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
    curl_setopt($curl, CURLOPT_USERAGENT, data::get('config.useragent'));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);
    }
    curl_close($curl);
    return $result;
}
/**
 * GET data
 */
function curl_get($url){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl,CURLOPT_USERAGENT,data::get('config.useragent'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
    $result = curl_exec($curl);
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);
    }
    curl_close($curl);
    return $result;
}
//截取中文字符串
function sub_str($string ,$sublen ,$prefix='' ,$start = 0){
    if (function_exists('mb_get_info')) {
        $length = mb_strlen($string, 'utf-8');
        $string = mb_substr($string, $start, $sublen, 'utf-8');
        return ($sublen < $length - $start) ? $string . $prefix : $string;
    } else {
        preg_match_all("/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/", $string, $t_string);
        if(count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen)).$prefix;
        return join('', array_slice($t_string[0], $start, $sublen));
    }
}
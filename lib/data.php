<?php
if(!defined('__ROOT__')) die ("Access Denied.");
/**
 * 缓存文件操作类(精简版)
 *
 * @package    Class
 * @author     Aries <helloaries@gmail.com>
 * @copyright  Copyright (c) 2010 (http://iw3c.com)
 */
class data{
    /**
     * 数据堆
     *
     * @access private
     * @var array
     */
    private static $_data = array();
    /**
     * 获取
     *
     * @param string $key
     * @access public
     * @return array|string
     */
    public static function get($key){
        if(!isset(self::$_data[$key])){
            @list($file,$arrayKey,$arrayVal) = explode('.',$key);
            $file = __DATA_ROOT__ . '/' .$file . '.php';
            if(file_exists($file)){
                self::$_data[$key] = require ($file);
            }else{
                return false;
            }
            if($arrayKey){
                if(!isset(self::$_data[$key][$arrayKey])) return false;
                if($arrayVal){
                    if(!isset(self::$_data[$key][$arrayKey][$arrayVal])) return false;
                    self::$_data[$key] = self::$_data[$key][$arrayKey][$arrayVal];
                }else{
                    self::$_data[$key] = self::$_data[$key][$arrayKey];
                }
            }
            unset($file,$arrayKey ,$arrayVal);
        }
        return self::$_data[$key];
    }
    /**
     * 写入
     *
     * @param array $data
     * @param string $id;
     * @param string $module
     * @access public
     * @return int
     */
    public static function set($data ,$id ,$module = null){
        $content = "";
        if(!is_array($data)){
            $data = (array)$data;
        }
        $content .= "<?php\n//缓存\n!defined('__ROOT__') && die ('Access Denied.');\nreturn ";
        $content .= var_export($data ,true);
        $content .= ";";
        $file = __DATA_ROOT__ . '/' .$id . '.php';
        return file_put_contents($file ,$content);
    }
}